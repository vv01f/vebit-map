#!/usr/bin/env sh
if="../non-public/eV/members.fods"
ofn=$(echo $if|rev|cut -d"/" -f1|cut -d"." -f2-|rev)

fae="file already exists."
# recv data

if test -e $ofn".csv" ; then
	echo "$fae"
else
	libreoffice --headless --convert-to csv:"Text - txt - csv (StarCalc)":"59,,76,3,1/1/2/2/2" $if || { echo "errors during export to csv"; exit 3; }
	tail -n +2 ${ofn}".csv" | cut -d";" -f2,13-17|grep -e "^[v|g|n]" > ${ofn}".tmp"
	mv ${ofn}".tmp" ${ofn}".csv" || { echo "error on removing first line."; exit 6; }
fi

