#!/usr/bin/env sh
echo "this takes some time depending on the amount of addresses in your lists …"
for file in *.csv ; do
	out=$(echo "${file}"|cut -d"." -f1)".geojson"
	if test -e "${out}" ; then
		echo "file exists: ${out}, skipping."
		continue
	fi
	echo ./lookup.py "${file}"
	./lookup.py "${file}" > "${out}"

	#~ fs=$(stat -t "${out}" |cut -d" " -f2)
	#~ data="vebit-geojson/"

	#~ if test "$fs" -gt 0 ; then
		#~ if test -d "$data" ; then 
			#~ cp -f "${out}" "$data"
			#~ cd "$data"
			#~ git commit "${out}" -m "update file" && git push
		#~ fi
	#~ fi

done
